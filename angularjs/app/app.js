'use strict';

// Declare app level module which depends on views, and components

var app = angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version',
]);

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.
    when('/view2', {
        templateUrl: '/view2/view2.html',
        controller: 'View2Ctrl'
    }).
    otherwise({
        redirect: '/view1/view1.html',
        controller: 'View1Ctrl'
    });
}]);

app.factory('integritasFactory', function () {
  return {
    run: function() {
    }
  }
});

app.controller('myController', [
  'integritasFactory',
  function(integritasFactory, $scope, $location){
    integritasFactory.run();
  }
]);